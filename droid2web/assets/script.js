$(function() {
    $("#content").width($(window).width() - 250);
    $("#content").height($(window).height() - 42);
    $("#contactlist").height($(window).height() - 42);
    $("#header").width($(window).width());
    
    $("#unreadcount").click(function() {getSmsUnread();});
    
    getSmsUnread();
    getSmsConversations();

});

$(window).resize(function() {
    $("#content").width($(window).width() - 250);
    $("#content").height($(window).height() - 42);
    $("#contactlist").height($(window).height() - 42);
});

function sendSms(addr, name) {
    console.log($("textarea#txtmessage").val());
    console.log(addr);
    $.post("sendsms", {address: addr, message: $("textarea#txtmessage").val()});
    getSmsConversation(addr, name, false);
    
}

function getSmsUnread() {
    $.get("getunread", function( response ){
        $("#unreadcount").html("");
        var lstUnread = JSON.parse( response );
        
        $("#unreadcount").html("<p>" + lstUnread.length + "</p>");
    });
}

function getSmsConversations() {
    $.get("getconversations", function( response ) {
        $("#contactlist").html("");
        var lstConversations = JSON.parse( response );
        for (var id in lstConversations) {
            var conversation = lstConversations[id];
            $("#contactlist").append("<li><a href=\"#\" onclick=\"getSmsConversation('" + conversation.address + "', '" + conversation.name + "', true)\">" + conversation.name + "</a></li>");
        }
    });
}

function unixdateToDate(udate) {
    var date = new Date(parseInt(udate));
    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}

function getSmsConversation( address, name, clear ) {
    if (clear = true) {
        $("#messages").html("<p>Loading conversation</p>");
    }
    
    $.get("getsmsconversation?addr=" + encodeURIComponent(address), function( response ) {
        var lstSms = JSON.parse( response );
        $("#messages").html("");
        for (var id in lstSms) {
            var sms = lstSms[id];
            
            var who;
            if(sms.folder == "sent") {
                who = "Me";
            } else {
                who = name;
            }
            $("#messages").append("<div class=\"smsmessage " + sms.folder + "\"><h1>" + who +  "</h1><p class='time'>" + unixdateToDate(sms.time) + "</p><p>" + sms.body + "</p></div>");
        }
        $('#writemessage').html("<form><textarea id=\"txtmessage\"></textarea><a href=\"#\" onclick=\"sendSms('" + sms.address + "', name)\">Send</a></form>");
        $('#content').scrollTop($('#content')[0].scrollHeight);
    });
}