var main;
var menu = $("<div id='menu'></div>");
    var btnnotifications = $("<div id='btnnotifications'></div>");
    var mouseinbtnnotifications = false;
    menu.append(btnnotifications);
    var btnsms = $("<div id='btnsms'><p>SMS</p></div>");
    menu.append(btnsms);
    var btncalls = $("<div id='btncalls'><p>Calls</p></div>");
    menu.append(btncalls);
var notifications = $("<div id='notifications'></div>");
var mouseinnotifications = false;
var content = $("<div id='content'></div>");
var contactlist = $("<div id='contactlist'></div>");
var messages = $("<div id='messages'></div>");
var writemessage = $("<form id='writemessage'><textarea id='messagefield'></textarea></form>");
var btnsendmessage = $("<p id='btnsendmessage'>Send</p>");

$(function () {
    
    // populate main view
    main = $("#main");
    main.append(menu);
    main.append(notifications);
    main.append(content);
    content.append(contactlist);
    content.append(messages);
    
    // set dom properties
    menu.width($(window).width());
    content.width($(window).width());
    content.height($(window).height() - 42);
    contactlist.height($(window).height() - 42);
    messages.width($(window).width() - 250);
    messages.height($(window).height() - 42);
    
    notifications.hide();
    contactlist.hide();
    messages.hide();
        
    btnsms.click(function () {
        setConversations(); contactlist.show(); messages.show();
        });
        
    btncalls.click(function () {
    	setCallLog();
    });
    
    $(window).mouseup(function (e) {

        if (!mouseinnotifications) {
        	if (mouseinbtnnotifications) {
        		notifications.toggle();
        	} else {
            	notifications.hide();
           	}
        }

        });
    
    notifications.hover(function () {
            mouseinnotifications = true;
        }, function () {
            mouseinnotifications = false;
        });
        
    btnnotifications.hover(function () {
            mouseinbtnnotifications = true;
        }, function () {
            mouseinbtnnotifications = false;
        });
    
    // show unread messages and missed calls
    setNotifications();
    
    //var getNotificationsPeriodically = setInterval(function(){setNotifications();}, 5000);

});

$(window).resize( function () {
    menu.width($(window).width());
    content.width($(window).width());
    content.height($(window).height() - 42);
    contactlist.height($(window).height() - 42);
    messages.width($(window).width() - 250);
    messages.height($(window).height() - 42);
});

setCallLog = function () {
	$.get("getcalllog", function ( response ) {
		console.log(response);
	});
}

setNotifications = function() {
    $.get("getnotifications", function( response ) {
        var lstMessages = JSON.parse(response);
        var lstSms = lstMessages['sms'];
        var lstCalls = lstMessages['calls'];
        var notificationcount = lstSms.length + lstCalls.length;
        btnnotifications.html("<p>" + notificationcount + "</p>");
        notifications.html("");
        for (var i in lstCalls) {
            call = lstCalls[i];
            divcall = $("<div class='notificationcall'></div>");
            divcall.html("<h1>At "+ unixtimeToDate(call.time) + " from " + call.name + "</h1>");
            notifications.append(divcall);
        }
        for (var i in lstSms) {
            sms = lstSms[i];
            divsms = $("<div class='notificationsms'></div>");
            divsms.html("<h1>At "+ unixtimeToDate(sms.time) + " from " +  sms.name + "</h1><p>" + sms.body + "</p>");
            notifications.append(divsms);
        }
    });
}

setConversations = function() {
    $.get("getconversations", function( response ) {
        var lstConversations = JSON.parse(response);
        contactlist.html("");
        for (var i in lstConversations) {
            var contact = lstConversations[i];
            
            var body = "";
            if (contact.body.length > 25) {
            	body = contact.body.substring(0, 25) + "...";
            } else {
            	body = contact.body;
            }
            var conversation = $("<div class='contact' onclick=\"setConversation('" + contact.address + "', '" + contact.name + "')\"></div>");
            conversation.html("<p class='contactname'>" + contact.name + "</p></p class='lastmessage'>" + body + "</p>");
            contactlist.append(conversation);
        }
    });
}

setConversation = function (address, name) {
        
    $.get("getconversation?addr=" + encodeURIComponent(address), function( response ) {
        var lstSms = JSON.parse( response );
        messages.html("");
        for (var i in lstSms) {
            var sms = lstSms[i];
            
            var who;
            if(sms.folder == "sent") {
                who = "Me";
            } else {
                who = name;
            }
            messages.append("<div class=\"smsmessage " + sms.folder + "\"><h1>" + who +  "</h1><p class='time'>" + unixtimeToDate(sms.time) + "</p><p>" + sms.body + "</p></div>");
        }
        
        messages.append(writemessage);
                $("textarea#messagefield").val("");
        writemessage.append(btnsendmessage);
        btnsendmessage.click(function () {sendSms(sms.address, name);});
        messages.scrollTop(messages[0].scrollHeight);
    });
}

sendSms = function (addr, name) {
	console.log(addr + ":" + name + ":" + $("textarea#messagefield").val());
	$.post("sendsms", {address: addr, message: $("textarea#messagefield").val()});
	$("textarea#messagefield").val("");
}

unixtimeToDate = function(udate) {
    var date = new Date(parseInt(udate));
    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}