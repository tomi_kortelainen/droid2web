package com.tomcool.droid2web;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.SmsManager;
import android.text.TextUtils;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

public class NanoServer extends NanoHTTPD {
    
    public static final String
        MIME_PLAINTEXT = "text/plain",
        MIME_HTML = "text/html",
        MIME_JS = "application/javascript",
        MIME_CSS = "text/css",
        MIME_PNG = "image/png",
        MIME_DEFAULT_BINARY = "application/octet-stream",
        MIME_XML = "text/xml";
    
    public Context ctx = null;

    public NanoServer(Context ctx) throws IOException {
        super(8888);
        this.ctx = ctx;
    }


    public String getContacts() {
        JSONArray lstContacts = new JSONArray();
        JSONObject objContact = new JSONObject();

        
        ContentResolver cr = ctx.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int total = cur.getCount();
        if(cur.moveToFirst()) {
            for (int i = 0; i < total; i++) {
                objContact = new JSONObject();
                
                try {
                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pcur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[] {cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID))},
                                null);
                        while(pcur.moveToNext()) {
                            objContact.put("phonenumber", pcur.getString(pcur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                            objContact.put("id", cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID)));
                            objContact.put("name", TextUtils.htmlEncode(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))));
                        }
                        pcur.close();
                        lstContacts.put(objContact);
                    }
                    

                    
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cur.moveToNext();
            }
        }
        cur.close();
        return lstContacts.toString();
    }
    
    public JSONArray getMissedCalls() {
        JSONArray lstCallLog = new JSONArray();
        JSONObject objCall = new JSONObject();
        
        Uri message = Uri.parse("content://call_log/calls");
        ContentResolver cr = ctx.getContentResolver();
        
        Cursor cur = cr.query(message, null, "new = 1 AND type = 3", null, null);
        
        int total = cur.getCount();
        
        if (cur.moveToFirst()) {
            for (int i = 0; i < total; i++) {
                objCall = new JSONObject();
                try {
                    objCall.put("address", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.NUMBER)));
                    objCall.put("time", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.DATE)));
                    objCall.put("read", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.NEW)));
                    objCall.put("name", 
                            TextUtils.htmlEncode(cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.CACHED_NAME))));
                    objCall.put("duration", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.DURATION)));
                    int type = Integer.parseInt(cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.TYPE)));
                    switch (type) {
                    case CallLog.Calls.INCOMING_TYPE:
                        objCall.put("type", "incoming");
                        break;
                    case CallLog.Calls.OUTGOING_TYPE:
                        objCall.put("type", "outgoing");
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        objCall.put("type", "missed");
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lstCallLog.put(objCall);
                cur.moveToNext();
            }
        }
        cur.close();
        
        return lstCallLog;
    }
    
    public String getCallLog() {
        JSONArray lstCallLog = new JSONArray();
        JSONObject objCall = new JSONObject();
        
        Uri message = Uri.parse("content://call_log/calls");
        ContentResolver cr = ctx.getContentResolver();
        
        Cursor cur = cr.query(message, null, null, null, null);
        
        int total = cur.getCount();
        
        if (cur.moveToFirst()) {
            for (int i = 0; i < total; i++) {
                objCall = new JSONObject();
                try {
                    objCall.put("address", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.NUMBER)));
                    objCall.put("time", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.DATE)));
                    objCall.put("read", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.NEW)));
                    objCall.put("name", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.CACHED_NAME)));
                    objCall.put("duration", 
                            cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.DURATION)));
                    int type = Integer.parseInt(cur.getString(cur.getColumnIndexOrThrow(CallLog.Calls.TYPE)));
                    switch (type) {
                    case CallLog.Calls.INCOMING_TYPE:
                        objCall.put("type", "incoming");
                        break;
                    case CallLog.Calls.OUTGOING_TYPE:
                        objCall.put("type", "outgoing");
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        objCall.put("type", "missed");
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lstCallLog.put(objCall);
                cur.moveToNext();
            }
        }
        cur.close();
        
        return lstCallLog.toString();
    }
    
    public String getSmsConversations() {
        JSONArray lstConversations = new JSONArray();
        JSONObject objConversation = new JSONObject();
        
        Uri message = Uri.parse("content://mms-sms/conversations");
        ContentResolver cr = ctx.getContentResolver();
        
        Cursor cur = cr.query(message, null, null, null, "date DESC");
        int total = cur.getCount();
        if (cur.moveToFirst()) {
            for(int i = 0; i < total; i++) {
                objConversation = new JSONObject();
                
                try {
                    String address = cur.getString(cur.getColumnIndexOrThrow("address"));
                    String body = "";
                    
                    String name;
                    objConversation.put("id",
                            cur.getString(cur.getColumnIndexOrThrow("_id")));
                    objConversation.put("address", address);
                    objConversation.put("time", cur.getString(cur.getColumnIndexOrThrow("date")));
                    name = address;
                    
                    if (cur.getString(cur.getColumnIndexOrThrow("body")) != null) {
                    	body = TextUtils.htmlEncode(cur.getString(cur.getColumnIndexOrThrow("body")));
                    }
                    
                    objConversation.put("body", body);

                    
                    Uri msg = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(address));
                    Cursor pcur = cr.query(msg, new String[]{PhoneLookup.DISPLAY_NAME}, "address = " + address, null, null);
                    if (pcur.moveToFirst()) {
                        name  =TextUtils.htmlEncode(pcur.getString(pcur.getColumnIndexOrThrow(PhoneLookup.DISPLAY_NAME)));
                    }
                    
                    objConversation.put("name", name);
                	
                	lstConversations.put(objConversation);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cur.moveToNext();
                
            }
        }
        cur.close();
        return lstConversations.toString();
    }
    
    
    public String getSmsConversation(String address) {
        
        JSONArray lstSms = new JSONArray();
        JSONObject objSms = new JSONObject();
        

        Uri message = Uri.parse("content://sms/");
        ContentResolver cr = ctx.getContentResolver();   
        
        Cursor cur = cr.query(message, null, "address = '" + address + "'", null, "date ASC");

        int totalSms = cur.getCount();

        if (cur.moveToFirst()) {
            for (int i = 0; i < totalSms; i++) {
                objSms = new JSONObject();
                
                try {
                    objSms.put("id",
                            cur.getString(cur.getColumnIndexOrThrow("_id")));
                    objSms.put("address",
                            cur.getString(cur.getColumnIndexOrThrow("address")));
                    objSms.put("body",
                    		TextUtils.htmlEncode(cur.getString(cur.getColumnIndexOrThrow("body"))));
                    objSms.put("read",
                            cur.getString(cur.getColumnIndex("read")));
                    objSms.put("time",
                            cur.getString(cur.getColumnIndexOrThrow("date")));
                    if (cur.getString(cur.getColumnIndexOrThrow("type"))
                            .contains("1")) {
                        objSms.put("folder", "inbox");
                    } else {
                        objSms.put("folder", "sent");
                    }
                    lstSms.put(objSms);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                
                cur.moveToNext();
            }
        }
        cur.close();
        return lstSms.toString();
    }
    
    public JSONArray getSmsUnread() {
        JSONArray lstUnread = new JSONArray();
        JSONObject objSms = new JSONObject();
      
        Uri message = Uri.parse("content://sms/inbox");
        ContentResolver cr = ctx.getContentResolver();
        
        Cursor cur = cr.query(message, null, "read = 0", null, "date DESC");
        int total = cur.getCount();
        if (cur.moveToFirst()) {
            for(int i = 0; i < total; i++) {
                objSms = new JSONObject();
                try {
                    String address = cur.getString(cur.getColumnIndexOrThrow("address"));
                    String name = address;
                    
                    objSms.put("id",
                            cur.getString(cur.getColumnIndexOrThrow("_id")));
                    objSms.put("address",
                            address);
                    objSms.put("body",
                    		TextUtils.htmlEncode(cur.getString(cur.getColumnIndexOrThrow("body"))));
                    objSms.put("time",
                            cur.getString(cur.getColumnIndexOrThrow("date")));
                    

                    Uri msg = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(address));
                    Cursor pcur = cr.query(msg, new String[]{PhoneLookup.DISPLAY_NAME}, "address = " + address, null, null);
                    if (pcur.moveToFirst()) {
                        name  = TextUtils.htmlEncode(pcur.getString(pcur.getColumnIndexOrThrow(PhoneLookup.DISPLAY_NAME)));
                    }
                    pcur.close();
                    
                    objSms.put("name", name);
                    lstUnread.put(objSms);
                    
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cur.moveToNext();
            }
        }
        cur.close();
        
        return lstUnread;
    }
    
    
    public Response sendSms(String address, String message) {
        SmsManager sms = SmsManager.getDefault();
        
        ArrayList<String> parts = sms.divideMessage(message);
        sms.sendMultipartTextMessage(address, null, parts, null, null);
        
        try {
            ContentValues values = new ContentValues();
            values.put("address", address);
            values.put("body", message);
            values.put("read", 1);
            values.put("date", System.currentTimeMillis());
            ctx.getContentResolver().insert(Uri.parse("content://sms/sent"), values);

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return new NanoHTTPD.Response("{\"OK\"}");
    }

    @Override
    public Response serve(String uri, Method method,
            Map<String, String> headers, Map<String, String> parms,
            Map<String, String> files) {
        

        if (uri.contains(".js")) {
            return serveFile(uri, MIME_JS);
            
        } else if(uri.contains(".css")) {
            return serveFile(uri, MIME_CSS);
            
            
        } else if (uri.equals("/getcontacts")) {
            return serveJson(getContacts());
            
        } else if (uri.equals("/getconversations")) {
            return serveJson(getSmsConversations());
            
        } else if (uri.equals("/fakesms")) {
            return serveJson(fakeSms(parms.get("address"), parms.get("message")));
            
        } else if (uri.equals("/getnotifications")) {
            return serveJson(getNotifications());
            
        } else if (uri.equals("/getcalllog")) {
            return serveJson(getCallLog());
            
        } else if (uri.equals("/getconversation")) {
            String address = parms.get("addr");
            return serveJson(getSmsConversation(address));
            
        } else if (uri.equals("/sendsms")) {
            return sendSms(parms.get("address"), parms.get("message"));
            
        } else {
            return serveFile("/index.html", MIME_HTML);
        }
    }
    

    public String fakeSms(String address, String message) {
        
        ContentValues content = new ContentValues();
        ContentResolver cr = ctx.getContentResolver();
        content.put("address", address);
        content.put("body", message);
        cr.insert(Uri.parse("content://sms/inbox"), content);
        
        
        return "{'OK'}";
    }


    public String getNotifications() {
        JSONArray lstSms = getSmsUnread();
        JSONArray lstCalls = getMissedCalls();
        JSONObject lstNotifications = new JSONObject();
        
        try {
            lstNotifications.put("calls", lstCalls);
            lstNotifications.put("sms", lstSms);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        
        return lstNotifications.toString();
    }


    public Response serveFile(String uri, String mime) {
        
        InputStream is = null;
        try {
            is = ctx.getAssets().open(uri.substring(1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return new NanoHTTPD.Response(Status.OK, mime, is);
    }
    
    public Response serveJson(String json) {
        return new NanoHTTPD.Response(Status.OK, MIME_PLAINTEXT, json);
    }

}