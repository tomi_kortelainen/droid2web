package com.tomcool.droid2web;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

    private NanoServer server;
	private TextView myText = null;

	public static String getLocalIp() {

		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
						return inetAddress.getHostAddress();
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	protected void onResume() {
		super.onResume();

		LinearLayout lView = new LinearLayout(this);
		myText = new TextView(this);
		myText.setText(getLocalIp());
		lView.addView(myText);
		setContentView(lView);

		try {
			server = new NanoServer(getApplicationContext());
			server.start();
		} catch(IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		super.onPause();

		if (server != null) {
			server.stop();
		}

	}
}
